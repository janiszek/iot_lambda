# NodeExample1
This example creates a basic DynamoDB table and loads 10 items into the table.  The table created in this example uses only a primary key "id", no sort key.  Primary key values must be unique.  

## Install Deps
```
npm install 
sudo apt install sqlite3 //unix/mac only
```

## Configure settings
```
1. Global settings for all apps are in settings.json.
2. Edit config.json to change accessKeyId, secretAccessKey, region for your subscription
3. Edit 3_populate_device_settings.js - update to change notifyEmail to your email address
4. Edit 4_create_sns_topic.sh - change email to your email address
5. On Windows 
  a. install GitBash
  b. Download jq version for Windows: curl -L -o c:\tmp\jq.exe https://github.com/stedolan/jq/releases/latest/download/jq-win64.exe and copy to your \git\cmd directory (which is on path)
```

## Create Lambda function
```
1. Create Lambda function with Role Template: Simple microservice permissions and AmazonSNSFullAccess added in IAM, API Gateway as trigger
2. Replace Lambda code with 5_lambda_function.js
```

## Run
```
1. node 1_create_settings_table.js
2. node 2_create_data_table.js
3. node 3_populate_device_settings.js (check data entered into DynamoDB table)
4. sh 4_create_sns_topic.sh (confirm registration eamil)
5. Update topicARN in 5_lambda_function.js with your SNS topic
6. Edit producer.js - change apiurl to your API Gateway API endpoint
7. node producer.js
```

# Clean up
```
1. sh deletetables.sh
2. Delete Lambda function iotingestion in AWS Console
3. SNS topic 'iotAlerts' and subscription in AWS Console
```

